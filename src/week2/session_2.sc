package week2

//
// Introduction to higher order functions
object session_2 {
  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumFunc(a: Int, b: Int): Int = {
      if (a > b) 0
      else f(a) + sumFunc(a + 1, b)
    }

    sumFunc
  }                                               //> sum: (f: Int => Int)(Int, Int) => Int

  def fact(x: Int): Int =
    if (x == 0) 1 else fact(x - 1)                //> fact: (x: Int)Int

  def sumInts = sum(x => x)                       //> sumInts: => (Int, Int) => Int
  def sumCubes = sum(x => x * x * x)              //> sumCubes: => (Int, Int) => Int
  def sumFactorials = sum(fact)                   //> sumFactorials: => (Int, Int) => Int

  //
  // EXERCISE 1
  // Product function
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)
  }                                               //> product: (f: Int => Int)(a: Int, b: Int)Int

  // Product of squares
  product(x => x * x)(3, 7)                       //> res0: Int = 6350400

  //
  // EXERCISE 2
  // Factorial in terms of product
  def pFact(x: Int) = product(x => x)(1, x)       //> pFact: (x: Int)Int
  pFact(4)                                        //> res1: Int = 24

  //
  // EXERCISE 3
  // Generalize sum and product
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
  	if(a > b) zero
  	else combine(f(a), mapReduce(f, combine, zero)(a + 1, b))
                                                  //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b:
                                                  //|  Int)Int
  
  def product2(f: Int => Int)(a: Int, b: Int): Int =
  	mapReduce(f, (x, y) => x * y, 1)(a, b)    //> product2: (f: Int => Int)(a: Int, b: Int)Int
}