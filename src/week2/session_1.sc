package week2

object session_1 {
  def sum(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, acc + f(a))
    }

    loop(a, 0)
  }                                               //> sum: (f: Int => Int)(a: Int, b: Int)Int

  def sumInts(from: Int, to: Int) =
    sum(x => x)(from, to)                         //> sumInts: (from: Int, to: Int)Int

  def sumCubes(from: Int, to: Int) =
    sum(x => x * x * x)(from, to)                 //> sumCubes: (from: Int, to: Int)Int

  def sumFactorials(from: Int, to: Int) = {
    def fact(x: Int): Int =
      if (x == 0) 1
      else fact(x - 1)

    sum(fact)(from, to)
  }                                               //> sumFactorials: (from: Int, to: Int)Int

  sumInts(1, 4)                                   //> res0: Int = 10
  sumCubes(1, 4)                                  //> res1: Int = 100
  sumFactorials(1, 4)                             //> res2: Int = 4
}