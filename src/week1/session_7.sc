package week1

object session_7 {
  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)              //> gcd: (a: Int, b: Int)Int

  def factorial(n: Int): Int =
    if (n == 0) 1 else n * factorial(n - 1)       //> factorial: (n: Int)Int
   
  def factorialTailRec(n: Int): Int = {
  	def tailRecurse(result: Int, n: Int): Int =
  		if(n == 0) result
  		else tailRecurse(result * n, n - 1)
  		
  	tailRecurse(1, n)
  }                                               //> factorialTailRec: (n: Int)Int
    
  gcd(14, 21)                                     //> res0: Int = 7
  factorial(4)                                    //> res1: Int = 24
  factorialTailRec(4)                             //> res2: Int = 24
}