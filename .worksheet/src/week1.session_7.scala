package week1

object session_7 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(104); 
  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b);System.out.println("""gcd: (a: Int, b: Int)Int""");$skip(77); 

  def factorial(n: Int): Int =
    if (n == 0) 1 else n * factorial(n - 1);System.out.println("""factorial: (n: Int)Int""");$skip(183); 
   
  def factorialTailRec(n: Int): Int = {
  	def tailRecurse(result: Int, n: Int): Int =
  		if(n == 0) result
  		else tailRecurse(result * n, n - 1)
  		
  	tailRecurse(1, n)
  };System.out.println("""factorialTailRec: (n: Int)Int""");$skip(19); val res$0 = 
    
  gcd(14, 21);System.out.println("""res0: Int = """ + $show(res$0));$skip(15); val res$1 = 
  factorial(4);System.out.println("""res1: Int = """ + $show(res$1));$skip(22); val res$2 = 
  factorialTailRec(4);System.out.println("""res2: Int = """ + $show(res$2))}
}
