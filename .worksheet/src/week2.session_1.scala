package week2

object session_1 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(207); 
  def sum(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, acc + f(a))
    }

    loop(a, 0)
  };System.out.println("""sum: (f: Int => Int)(a: Int, b: Int)Int""");$skip(64); 

  def sumInts(from: Int, to: Int) =
    sum(x => x)(from, to);System.out.println("""sumInts: (from: Int, to: Int)Int""");$skip(73); 

  def sumCubes(from: Int, to: Int) =
    sum(x => x * x * x)(from, to);System.out.println("""sumCubes: (from: Int, to: Int)Int""");$skip(147); 

  def sumFactorials(from: Int, to: Int) = {
    def fact(x: Int): Int =
      if (x == 0) 1
      else fact(x - 1)

    sum(fact)(from, to)
  };System.out.println("""sumFactorials: (from: Int, to: Int)Int""");$skip(18); val res$0 = 

  sumInts(1, 4);System.out.println("""res0: Int = """ + $show(res$0));$skip(17); val res$1 = 
  sumCubes(1, 4);System.out.println("""res1: Int = """ + $show(res$1));$skip(22); val res$2 = 
  sumFactorials(1, 4);System.out.println("""res2: Int = """ + $show(res$2))}
}
