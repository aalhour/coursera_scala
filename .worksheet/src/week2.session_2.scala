package week2

//
// Introduction to higher order functions
object session_2 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(247); 
  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumFunc(a: Int, b: Int): Int = {
      if (a > b) 0
      else f(a) + sumFunc(a + 1, b)
    }

    sumFunc
  };System.out.println("""sum: (f: Int => Int)(Int, Int) => Int""");$skip(63); 

  def fact(x: Int): Int =
    if (x == 0) 1 else fact(x - 1);System.out.println("""fact: (x: Int)Int""");$skip(30); 

  def sumInts = sum(x => x);System.out.println("""sumInts: => (Int, Int) => Int""");$skip(37); 
  def sumCubes = sum(x => x * x * x);System.out.println("""sumCubes: => (Int, Int) => Int""");$skip(32); 
  def sumFactorials = sum(fact);System.out.println("""sumFactorials: => (Int, Int) => Int""");$skip(157); 

  //
  // EXERCISE 1
  // Product function
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)
  };System.out.println("""product: (f: Int => Int)(a: Int, b: Int)Int""");$skip(54); val res$0 = 

  // Product of squares
  product(x => x * x)(3, 7);System.out.println("""res0: Int = """ + $show(res$0));$skip(102); 

  //
  // EXERCISE 2
  // Factorial in terms of product
  def pFact(x: Int) = product(x => x)(1, x);System.out.println("""pFact: (x: Int)Int""");$skip(11); val res$1 = 
  pFact(4);System.out.println("""res1: Int = """ + $show(res$1));$skip(227); 

  //
  // EXERCISE 3
  // Generalize sum and product
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
  	if(a > b) zero
  	else combine(f(a), mapReduce(f, combine, zero)(a + 1, b));System.out.println("""mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int)Int""");$skip(98); 
  
  def product2(f: Int => Int)(a: Int, b: Int): Int =
  	mapReduce(f, (x, y) => x * y, 1)(a, b);System.out.println("""product2: (f: Int => Int)(a: Int, b: Int)Int""")}
}
